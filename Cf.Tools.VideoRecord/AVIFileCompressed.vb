﻿Imports System.Drawing
Imports System.Drawing.Imaging
Imports System.IO

''' <summary> 
''' Класс для записи сжатого видео. 
''' </summary>
Public Class AviFileCompressed

    Private _width As Integer = 0
    Private _height As Integer = 0
    Private _firstBmp As Boolean = False
    Private _aviManager As AviManager
    Private _aviStream As VideoStream
    Private ReadOnly _locker As New Object()
    Private ReadOnly _codec As VideoCodecInfo
    Private ReadOnly _fps As Double
    Private ReadOnly _fname As String

    ''' <summary>Класс для записи сжатого видео. </summary>
    ''' <param name="fname">Путь к файлу.</param>
    ''' <param name="fps">Приблизительное число кадров в секунду.</param>
    ''' <param name="codec">Кодек. Если null, то используется VideoCodecInfo.CurrentCodec.</param>
    Public Sub New(ByVal fname As String, ByVal fps As Double, ByVal codec As VideoCodecInfo)
        _codec = codec
        _fps = fps
        _fname = fname
        Try
            SyncLock _locker
                _aviManager = New AviManager(_fname, False)
            End SyncLock
        Catch exc As Exception
            Throw New Exception("AviFileCompressed.New. ", exc)
        End Try
    End Sub


    ''' <summary> Размер текущего видеофайла в байтах. </summary>
    Public ReadOnly Property FileSize() As Long
        Get
            Dim size = CLng(0)
            Try
                If File.Exists(_fname) Then
                    Dim info = New FileInfo(_fname)
                    size = info.Length
                End If
            Catch exc As Exception
                'Log.Error("AviFileCompressed.FileSize", exc)
                Throw New Exception("AviFileCompressed.FileSize", exc)
            End Try
            Return size
        End Get
    End Property

    ''' <summary> Добавить кадр в видеофайл. </summary>
    ''' <param name="bmp">Кадр. Формат изображения должен быть Format24bppRgb или Format32bppArgb.</param>
    Public Sub AddFrame(ByVal bmp As Bitmap)
        Try
            SyncLock _locker
                If _aviManager Is Nothing Then
                    Return
                End If
                If bmp IsNot Nothing Then
                    If bmp.PixelFormat <> PixelFormat.Format32bppArgb Then
                        bmp = bmp.Clone(New Rectangle(0, 0, bmp.Width, bmp.Height), PixelFormat.Format32bppArgb)
                    End If
                    If Not _firstBmp Then
                        _aviStream = _aviManager.AddVideoStream(True, _fps, bmp, _codec)
                        _firstBmp = True
                        _width = bmp.Width
                        _height = bmp.Height
                    Else
                        If _width = bmp.Width AndAlso _height = bmp.Height Then
                            _aviStream.AddFrame(bmp)
                        End If
                    End If
                End If
            End SyncLock
        Catch exc As Exception
            Close()
            Throw New Exception("AVIFileCompressed.AddFrame", exc)
        End Try
    End Sub

    ''' <summary> Закрыть и сохранить видеофайл. </summary>
    Public Sub Close()
        Try
            SyncLock _locker
                If _aviManager Is Nothing Then
                    Return
                End If
                _aviManager.Close()
                _aviStream = Nothing
                _aviManager = Nothing
            End SyncLock
        Catch exc As Exception
            Throw New Exception("AVIFileCompressed.Close", exc)
        End Try
    End Sub

    Public ReadOnly Property IsClosed() As Boolean
        Get
            Return _aviManager Is Nothing
        End Get
    End Property

End Class
