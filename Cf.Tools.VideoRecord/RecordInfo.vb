﻿Public Class RecordInfo
	Public Property Name As String
	Public Property Dir As String
	Public Property Fps As Double
	Public Property Codec As VideoCodecInfo
    Public Property RecordCreationDate As Date
    Public Property Channel As String

    Public Sub New()

    End Sub

    Public Sub New(setName As String, setDir As String, setFps As Double, setCodec As VideoCodecInfo,
            setRecordCreationDate As Date, setChannel As String)
        Name = setName
        Dir = setDir
        Fps = setFps
        Codec = setCodec
        RecordCreationDate = setRecordCreationDate
        Channel = setChannel
    End Sub
End Class