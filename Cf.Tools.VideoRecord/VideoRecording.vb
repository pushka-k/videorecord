﻿Imports System.Drawing
Imports System.IO

Public Class VideoRecording
    Private ReadOnly _recordInfo As RecordInfo
    Private _aviFileCompressed As AVIFileCompressed
    Private _filename As String
    Private _framesCount As Integer
    Private _lastFileCrationTime As DateTime

    ''' <summary>
    ''' Формат даты
    ''' </summary>
    ''' <returns></returns>
    Public Shared ReadOnly Property DateTimeFormat As String
        Get
            Return "dd.MM.yyyy HH-mm-ss"
        End Get
    End Property

    ''' <summary>
    ''' Имя файла
    ''' </summary>
    ''' <returns></returns>
    Public ReadOnly Property FileName As String
        Get
            Return _filename
        End Get
    End Property

    ''' <summary>
    ''' Количество кадров
    ''' </summary>
    ''' <returns></returns>
    Public ReadOnly Property FramesCount
        Get
            Return _framesCount
        End Get
    End Property

    ''' <summary>
    ''' Видеозапись
    ''' </summary>
    ''' <returns></returns>
    Public ReadOnly Property RecordInfo As RecordInfo
        Get
            Return _recordInfo
        End Get
    End Property

    ''' <summary>
    ''' Видеозапись
    ''' </summary>
    ''' <param name="recordInfo"></param>
    Public Sub New(recordInfo As RecordInfo)
        _recordInfo = recordInfo
        _recordInfo.RecordCreationDate = Now()
        If _recordInfo.name = Nothing Then
            _recordInfo.name = "noname"
        End If
        Dim dir = Path.Combine(_recordInfo.dir, _recordInfo.name)
        _recordInfo.dir = dir + "\"
        If Not Directory.Exists(_recordInfo.dir) Then
            Directory.CreateDirectory(_recordInfo.dir)
        End If
        Try
            StartRecord()
        Catch exc As Exception
            Throw New Exception("VideoRecording.New", exc)
        End Try
    End Sub

    ''' <summary>
    ''' Начало записи
    ''' </summary>
    Public Sub StartRecord()
        _filename = NewVideoFileName()
        _lastFileCrationTime = Now
        Dim path = IO.Path.Combine(_recordInfo.Dir, _filename + ".avi")
        _aviFileCompressed = New AviFileCompressed(path, _recordInfo.Fps, _recordInfo.Codec)
    End Sub

    ''' <summary>
    ''' Новое имя видеофайла
    ''' </summary>
    ''' <returns></returns>
    Private Function NewVideoFileName() As String
        Dim nameOfFile = _recordInfo.name + "_" + Now().ToString(DateTimeFormat)
        Dim path = _recordInfo.Dir + nameOfFile + ".avi"

        Try
            If Not Directory.Exists(_recordInfo.Dir) Then
                Directory.CreateDirectory(_recordInfo.Dir)
            End If
        Catch ex As Exception
        End Try

        If File.Exists(path) Then
            Throw New Exception("VideoRecording.NewVideoFileName. Файл с таким именем уже существует.")
        End If
        Return nameOfFile
    End Function

    ''' <summary>
    ''' Переименование видеофайла
    ''' </summary>
    Private Sub RenameVideoFile()
        Dim result As String
        Dim path = IO.Path.Combine(_recordInfo.Dir, _filename + ".avi")
        If File.Exists(path) Then
            Dim subnames = _filename.Split("_"c)
            If subnames.Length = 2 Then
                result = _filename + "_" + DateTime.Now().ToString(DateTimeFormat)
            Else
                Throw New Exception("VideoRecording.VidefileName.")
            End If
            Dim path2 = _recordInfo.dir + result + ".avi"
            File.Move(path, path2)
        End If
    End Sub

    ''' <summary>
    ''' Добавление кадра
    ''' </summary>
    ''' <param name="bmp"></param>
    Public Sub AddFrame(bmp As Bitmap, TimeToRecordVideo As TimeSpan)
        If _aviFileCompressed Is Nothing Then
            Close()
            StartRecord()
        End If
        If _aviFileCompressed IsNot Nothing AndAlso bmp IsNot Nothing Then
            _aviFileCompressed.AddFrame(bmp)
            _framesCount += 1
            Dim timeRec = Now - _lastFileCrationTime
            If timeRec >= TimeToRecordVideo Then
                Close()
                StartRecord()
            End If
        End If
    End Sub

    ''' <summary>
    ''' Закрытие
    ''' </summary>
    Public Sub Close()
        If _AVIFileCompressed IsNot Nothing Then
            _AVIFileCompressed.Close()
            _AVIFileCompressed = Nothing
        End If
        _framesCount = 0
        RenameVideoFile()
    End Sub
End Class
