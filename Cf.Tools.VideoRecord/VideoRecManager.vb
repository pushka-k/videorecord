﻿Imports System.Drawing
Imports System.Globalization
Imports System.IO
Imports System.Threading

Public Class VideoRecManager
    Implements IDisposable

    Private ReadOnly _videoRecordings As New List(Of VideoRecording)
    Private _lifetimeControlThread As Thread
    Private _disposeHandle As ManualResetEvent
    Private _disposing As Boolean = False

    Public Event Logger(msg As String)
    Public Event LoggerWithType(msgType As String, msg As String)

    Public Property Codec As String = "1684633208"
    Public Property DirectoryForSaving As String
    Public Property TimeToStoreVideoHours As Integer = 24
    Public Property VideoLengthMinutes As Integer = 20
    Public Property Fps As Double = 5

    Private ReadOnly Property VideoRecordings As IEnumerable(Of VideoRecording)
        Get
            Return _videoRecordings
        End Get
    End Property

    ''' <summary>Менеджер видеозаписей</summary>
    Public Sub New()
        StartRemoveOldRecordsThread()
    End Sub

    Private Sub StartRemoveOldRecordsThread()
        _disposeHandle = New ManualResetEvent(False)
        _lifetimeControlThread = New Thread(AddressOf RemoveOldRecordsThread)
        _lifetimeControlThread.Priority = ThreadPriority.BelowNormal
        _lifetimeControlThread.IsBackground = True
        _lifetimeControlThread.Start()
    End Sub

    Private Sub RemoveOldRecordsThread()
        Do
            Dim videoRecords As IEnumerable(Of VideoRecording)
            SyncLock (_videoRecordings)
                videoRecords = _videoRecordings.ToArray
            End SyncLock
            If videoRecords IsNot Nothing Then
                For Each video In videoRecords
                    Try
                        Dim dir = video.RecordInfo.Dir
                        If Directory.Exists(dir) Then
                            Dim files = Directory.GetFiles(dir, "*.avi")
                            For Each file In files
                                If _disposing Then
                                    Exit Do
                                End If
                                Dim filename = Path.GetFileNameWithoutExtension(file)
                                Dim subfilename = filename.Split("_"c)
                                If subfilename.Length = 3 Then
                                    Dim time = subfilename(1)
                                    Dim creationTime = DateTime.ParseExact(time, VideoRecording.DateTimeFormat,
                                                                           CultureInfo.InvariantCulture)
                                    Dim nowTime = Date.Now
                                    Dim timeSpanHours = (nowTime - creationTime).TotalHours
                                    If timeSpanHours >= TimeToStoreVideoHours Then
                                        Try
                                            IO.File.Delete(file)
                                        Catch ex As Exception
                                            RaiseEvent LoggerWithType("err",
                                                    "VideoRecManager.RemoveOldRecordsThread - ошибка удаления файла видеозаписи " +
                                                    file + vbCrLf + ex.Message)
                                        End Try
                                    End If
                                End If
                            Next
                        End If
                    Catch ex As Exception
                        RaiseEvent LoggerWithType("err",
                                "VideoRecManager.RemoveOldRecordsThread - ошибка удаления файлов видеозаписи " +
                                video.RecordInfo.Name + vbCrLf + ex.Message)
                    End Try
                Next
            End If
            Dim index = WaitHandle.WaitAny({_disposeHandle}, New TimeSpan(0, 30, 0), False)
            If index <> WaitHandle.WaitTimeout Then
                Exit Do
            End If
        Loop
    End Sub

    ''' <summary>
    ''' Добавление новой видеозаписи
    ''' </summary>
    ''' <param name="recordInfo">Информация о видеозаписи</param>
    Private Sub AddVideoRec(recordInfo As RecordInfo)
        SyncLock (_videoRecordings)
            Dim findRec = _videoRecordings.Find(Function(x) x.RecordInfo.Name = recordInfo.Name)
            If findRec Is Nothing Then
                Dim newVideoRecording = New VideoRecording(recordInfo)
                If newVideoRecording IsNot Nothing Then
                    _videoRecordings.Add(newVideoRecording)
                Else
                    Throw New Exception("VideoRecManager.AddVideoRec. Не удалось добавить видеозапись.", Nothing)
                End If
            Else
                Throw _
                    New Exception(String.Format("Видеозапись с именем {0} уже существует.",
                                                """" + recordInfo.Name + """"))
            End If
        End SyncLock
    End Sub

    Public Sub StopRecord(channel As String)
        SyncLock (_videoRecordings)
            If _videoRecordings IsNot Nothing Then
                Dim videoRec = _videoRecordings.Find(Function(x) x.RecordInfo.Channel = channel)
                If videoRec Is Nothing Then
                    videoRec.Close()
                End If
            End If
        End SyncLock
    End Sub

    ''' <summary>
    '''     Добавление кадра в определённый канал
    ''' </summary>
    ''' <param name="channel">Канал</param>
    ''' <param name="bmp">Кадр</param>
    Public Sub AddFrame(channel As String, bmp As Bitmap)
        SyncLock (_videoRecordings)
            If _videoRecordings IsNot Nothing Then
                Dim videoRec = _videoRecordings.Find(Function(x) x.RecordInfo.Channel = channel)

                If videoRec Is Nothing Then
                    Dim newVideoRec = New RecordInfo
                    With newVideoRec
                        .Channel = channel
                        .Codec = VideoCodecInfo.GetCodec(Codec)
                        .Dir = _DirectoryForSaving
                        .Fps = _fps
                        .Name = channel
                        .RecordCreationDate = Now
                    End With

                    AddVideoRec(newVideoRec)

                    videoRec = _videoRecordings.Find(Function(x) x.RecordInfo.Channel = channel)
                End If

                If videoRec IsNot Nothing Then
                    Try
                        videoRec.AddFrame(bmp, New TimeSpan(0, VideoLengthMinutes, 0))
                        GC.Collect() ' Костыль, но без него занимаемая память стремится к бесконечности.
                    Catch ex As Exception
                        Throw New Exception("VideoRecManager.AddFrame", ex)
                    End Try
                End If
            End If
        End SyncLock
    End Sub

    ''' <summary>
    ''' Закрытие менеджера
    ''' </summary>
    Public Sub Dispose() Implements IDisposable.Dispose
        _disposeHandle.Set()
        _disposing = True
        For Each videoRec In _videoRecordings.ToArray
            Try
                videoRec.Close()
            Catch ex As Exception
                RaiseEvent LoggerWithType("err", "VideoRecManager.Dispose " + videoRec.RecordInfo.Name + vbCrLf +
                            ex.Message)
            End Try
        Next
    End Sub

    Public Sub CloseAllRecords()
        SyncLock (_videoRecordings)
            For Each videoRec In _videoRecordings.ToArray
                Try
                    videoRec.Close()
                Catch ex As Exception
                    RaiseEvent LoggerWithType("err", "VideoRecManager.CloseAllRecords " + videoRec.RecordInfo.Name + vbCrLf +
                            ex.Message)
                End Try
            Next
        End SyncLock
    End Sub

    Public Sub RestartAllRecords()
        SyncLock (_videoRecordings)
            For Each videoRec In _videoRecordings.ToArray
                Try
                    videoRec.Close()
                    videoRec.StartRecord()
                Catch ex As Exception
                    RaiseEvent LoggerWithType("err", "VideoRecManager.RestartAllRecords " + videoRec.RecordInfo.Name + vbCrLf +
                            ex.Message)
                End Try
            Next
        End SyncLock
    End Sub
End Class
