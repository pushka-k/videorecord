﻿Imports System.IO
Imports System.Text

''' <summary> Класс для работы с кодеками для сжатия видео. </summary>
''' <remarks>
'''     Для выбора кодека:
'''     1) в конструктор AVIFileCompressed передать null, тогда будет использоваться VideoCodecInfo.CurrentCodec.
'''     2) получить кодек с помщью VideoCodecInfo.GetCodec или VideoCodecInfo.ChooseCompressorSTD и передать его в конструктор AVIFileCompressed.
''' </remarks>
Public Class VideoCodecInfo
    Public Shared ReadOnly Codecs As New List(Of VideoCodecInfo)()
    Private Shared _videoCodecFccHandler As VideoCodecInfo
    Public Property Name As String
    Public Property FourCc As UInteger
    Public Shared ReadOnly Property CurrentCodec As VideoCodecInfo
        Get
            Return _videoCodecFCCHandler
        End Get
    End Property
    Public Shared ReadOnly Property Uncompressed As VideoCodecInfo
        Get
            SyncLock Codecs
                Return Codecs.Find(Function(c) c.FourCC = 541215044)
            End SyncLock
        End Get
    End Property
    Public Shared ReadOnly Property Xvid As VideoCodecInfo
        Get
            SyncLock Codecs
                Return Codecs.Find(Function(c) c.FourCC = 1684633208)
            End SyncLock
        End Get
    End Property
    ''' <summary>
    ''' Инициализация списка кодеков
    ''' </summary>
    Shared Sub New()
        'TODO: НЕ факт, что эти кодеки установлены в системе! Это просто сделано на будущее, чтобы управлять без графического интерфейса.

        SyncLock Codecs
            Codecs.Add(New VideoCodecInfo(541215044, "Без сжатия"))
            Codecs.Add(New VideoCodecInfo(1684633208, "XVID"))
            Codecs.Add(New VideoCodecInfo(1701605997, "Microsoft RLE"))
            Codecs.Add(New VideoCodecInfo(1668707181, "Microsoft Video 1"))
            Codecs.Add(New VideoCodecInfo(2021026148, "DIVX_"))
        End SyncLock

        _videoCodecFCCHandler = Codecs(0)
    End Sub
    ''' <summary> Конструктор. </summary>
    ''' <param name="fourcc">Код кодека.</param>
    ''' <param name="name">Имя кодека.</param>
    Public Sub New(fourcc As UInteger, name As String)
        Me.FourCC = fourcc
        Me.Name = name
    End Sub
    ''' <summary>
    ''' Отображение стандартного окна настроек Windows для выбора и настройки кодека. Отображаются кодеки, установленные в системе.
    ''' </summary>
    ''' <param name="hwnd">HWND родительского окна.</param>
    ''' <returns>Выбранный кодек или null.</returns>
    Public Shared Function ChooseCompressorStd(hwnd As IntPtr) As VideoCodecInfo
        Try
            Dim filenameDest = Path.Combine(AppDomain.CurrentDomain.BaseDirectory, "tmp.avi")
            AVIAPI.AVIFileInit()
            Dim aviFile As IntPtr
            Aviapi.AVIFileOpen(aviFile, filenameDest, AviFileMode.OfWrite Or AviFileMode.OfCreate, 0)

            Dim aviStream As IntPtr = Nothing

            Dim strhdr As New Aviapi.Avistreaminform()
            strhdr.fccType = FssType.StreamtypeVideo
            strhdr.fccHandler = Aviapi.mmioStringToFOURCC("CVID", 0)
            strhdr.dwScale = 1
            strhdr.dwRate = 25 ' FPS
            strhdr.dwQuality = -1
            strhdr.dwSampleSize = 0
            strhdr.rcFrame.top = 0
            strhdr.rcFrame.left = 0
            strhdr.dwEditCount = 0
            strhdr.dwFormatChangeCount = 0
            strhdr.szName = New Char(63) {}

            Aviapi.AVIFileCreateStream(aviFile, aviStream, strhdr)

            Dim options As New Aviapi.AvicompressoptionsClass()
            options.fccType = CUInt(FssType.StreamtypeVideo)

            options.lpParms = IntPtr.Zero
            options.lpFormat = IntPtr.Zero

            'пользователь выбирает параметры сжатия
            Aviapi.AVISaveOptions(hwnd, Aviapi.IcmfChooseKeyframe Or Aviapi.IcmfChooseDatarate, 1, aviStream,
                                  options)
            Aviapi.AVISaveOptionsFree(1, options)

            Dim tmp = GetCodec(options.fccHandler)

            AVIAPI.AVIStreamRelease(aviStream)
            AVIAPI.AVIFileRelease(aviFile)
            AVIAPI.AVIFileExit()

            Return tmp
        Catch exc As Exception
            Throw New Exception("VideoCodecInfo.ChooseCompressorSTD", exc)
        End Try
    End Function


    ''' <summary> Установить текущий кодек VideoCodecInfo.CurrentCodec. </summary>
    ''' <param name="fourcc">Код кодека.</param>
    ''' <returns>True при успехе.</returns>
    Public Shared Function SetCurrentCodec(fourcc As UInteger) As Boolean
        Dim codec = GetCodec(fourcc)
        If codec Is Nothing Then
            Return False
        End If
        _videoCodecFCCHandler = codec
        Return True
    End Function

    ''' <summary>
    '''     Преобразование кода кодека в строку. Обратная процедура есть в
    ''' </summary>
    ''' <param name="fourcc">FourCC код.</param>
    ''' <returns>Строковое представление FourCC код.</returns>
    Public Shared Function FourCcToString(fourcc As UInteger) As String
        Try
            Dim bytes(3) As Byte
            bytes(0) = CByte(fourcc And &HFF)
            fourcc = fourcc >> 8
            bytes(1) = CByte(fourcc And &HFF)
            fourcc = fourcc >> 8
            bytes(2) = CByte(fourcc And &HFF)
            fourcc = fourcc >> 8
            bytes(3) = CByte(fourcc And &HFF)
            Return Encoding.ASCII.GetString(bytes)
        Catch exc As Exception
            Return String.Empty
        End Try
    End Function

    ''' <summary> Получить кодек по его коду. </summary>
    ''' <param name="fourcc">FourCC код.</param>
    ''' <returns>Кодек.</returns>
    ''' <remarks>Если нужного кодека нет в списке, то он автоматически доваляется.</remarks>
    Public Shared Function GetCodec(fourcc As UInteger) As VideoCodecInfo
        If fourcc = 0 Then
            Return Nothing
        End If
        Dim res As VideoCodecInfo

        SyncLock Codecs
            res = Codecs.Find(Function(c) c.FourCC = fourcc)
        End SyncLock
        If res Is Nothing Then
            res = AddCodec(fourcc)
        End If
        Return res
    End Function

    ''' <summary> Добавить кодек к списку кодеков. </summary>
    ''' <param name="fourcc">FourCC код.</param>
    ''' <returns>Новый кодек.</returns>
    Private Shared Function AddCodec(fourcc As UInteger) As VideoCodecInfo
        SyncLock Codecs
            Try
                If Codecs.Find((Function(c) c.FourCC = fourcc)) Is Nothing Then
                    'INSTANT VB NOTE: The variable name was renamed since Visual Basic does not handle local variables named the same as class members well:
                    Dim nameRenamed = FourCCToString(fourcc)
                    Dim codec = New VideoCodecInfo(fourcc, nameRenamed)
                    Codecs.Add(codec)
                    Return codec
                End If
            Catch exc As Exception
                Throw New Exception("VideoCodecInfo", exc)
            End Try
        End SyncLock
        Return Nothing
    End Function
    Public Overrides Function ToString() As String
        Return Name
    End Function
    Public Shared Function GetCodecs() As List(Of VideoCodecInfo)
        Return Codecs
    End Function
End Class
