﻿Imports System
Imports System.Runtime.InteropServices
Imports System.Security
Imports System.Text

''' <summary>Код ошибки.</summary>
Public Enum AviErrorCode As UInteger
	AvierrUnknown = &HFFFFFFFFUI

	AvierrOk = &H0

	AvierrUnsupported = &H80044065UI
	AvierrBadformat = &H80044066UI
	AvierrMemory = &H80044067UI
	AvierrInternal = &H80044068UI
	AvierrBadflags = &H80044069UI
	AvierrBadparam = &H8004406AUI
	AvierrBadsize = &H8004406BUI
	AvierrBadhandle = &H8004406CUI
	AvierrFileread = &H8004406DUI
	AvierrFilewrite = &H8004406EUI
	AvierrFileopen = &H8004406FUI
	AvierrCompressor = &H80044070UI
	AvierrNocompressor = &H80044071UI
	AvierrReadonly = &H80044072UI
	AvierrNodata = &H80044073UI
	AvierrBuffertoosmall = &H80044074UI
	AvierrCantcompress = &H80044075UI
	AvierrUserabort = &H800440C6UI
	AvierrError = &H800440C7UI
	RegdbEClassnotreg = &H80040154UI
End Enum

<Flags>
Friend Enum AviFileMode As UInteger
	OfRead = &H0
	OfWrite = &H1
	OfReadwrite = &H2
	OfShareExclusive = &H10
	OfShareDenyWrite = &H20
	OfShareDenyRead = &H30
	OfShareDenyNone = &H40
	OfCreate = &H1000
End Enum

Friend Enum FssType As UInteger
	StreamtypeVideo = &H73646976 ' mmioStringToFOURCC("vids", 0)
End Enum

Friend Module FourCc
	Private Const FOURCC_LENGHT As Integer = 4

	<System.Runtime.CompilerServices.Extension> _
	Public Function IsEqualFourCc(ByVal code As UInteger, ByVal mmio As String) As Boolean
		Return ToText(code).Equals(mmio.PadRight(FOURCC_LENGHT), StringComparison.OrdinalIgnoreCase)
	End Function

	Public Function FromText(ByVal code As String) As UInteger
		If code.Length <> FOURCC_LENGHT Then
			Throw New ArgumentException("Строка FourCC должна содержать 4 символа.")
		End If
		Dim bytes = Encoding.ASCII.GetBytes(code.ToCharArray())
		Dim c = 0L
		Dim m = 1L
		For Each t In bytes
			c += m * t
			m *= 256
		Next t
		Return CUInt(c)
	End Function

	Public Function ToText(ByVal code As UInteger) As String
		Dim bytes = New Byte(FOURCC_LENGHT - 1) {}
		For i = 0 To bytes.Length - 1
			bytes(i) = CByte(code Mod 256)
			code \= 256
		Next i
		Return New String(Encoding.ASCII.GetChars(bytes))
	End Function
End Module

<SuppressUnmanagedCodeSecurity>
Friend NotInheritable Class Aviapi

	Private Sub New()
	End Sub
#Region "Const"


	Public Const BmpMagicCookie As Integer = &H4D42 'ascii string "BM"

	Public Const AviifKeyframe As Integer = &H10

	Public Const BiRgb As UInteger = 0
	Public Const BiRle8 As UInteger = 1
	Public Const BiRle4 As UInteger = 2
	Public Const BiBitfields As UInteger = 3
	Public Const BiJpeg As UInteger = 4
	Public Const BiPng As UInteger = 5

	Public Const IcmfChooseKeyframe As UInt32 = &H1 ' show KeyFrame Every box
	Public Const IcmfChooseDatarate As UInt32 = &H2 ' show DataRate box
	Public Const IcmfChoosePreview As UInt32 = &H4 ' allow expanded preview dialog

#End Region

#Region "Structures"

	<StructLayout(LayoutKind.Sequential, Pack:=1)>
	Public Structure Rect
		Public left As UInt32
		Public top As UInt32
		Public right As UInt32
		Public bottom As UInt32
	End Structure

	<StructLayout(LayoutKind.Sequential, Pack:=1)>
	Public Structure Bitmapfileheader
		Public bfType As Int16 '"magic cookie" - must be "BM"
		Public bfSize As Int32
		Public bfReserved1 As Int16
		Public bfReserved2 As Int16
		Public bfOffBits As Int32
	End Structure

	<StructLayout(LayoutKind.Sequential, Pack:=1)>
	Public Structure Avicompressoptions
		Public fccType As UInt32
		Public fccHandler As UInt32
		Public dwKeyFrameEvery As UInt32 ' only used with AVICOMRPESSF_KEYFRAMES
		Public dwQuality As UInt32
		Public dwBytesPerSecond As UInt32 ' only used with AVICOMPRESSF_DATARATE
		Public dwFlags As UInt32
		Public lpFormat As IntPtr
		Public cbFormat As UInt32
		Public lpParms As IntPtr
		Public cbParms As UInt32
		Public dwInterleaveEvery As UInt32
	End Structure

	<StructLayout(LayoutKind.Sequential, Pack:=1)>
	Public Structure Bitmapinfoheader
		Public biSize As UInt32
		Public biWidth As Int32
		Public biHeight As Int32
		Public biPlanes As UInt16
		Public biBitCount As UInt16
		Public biCompression As UInt32
		Public biSizeImage As UInt32
		Public biXPelsPerMeter As Int32
		Public biYPelsPerMeter As Int32
		Public biClrUsed As UInt32
		Public biClrImportant As UInt32
	End Structure

	<StructLayout(LayoutKind.Sequential, Pack:=1)>
	Public Structure Avifileinform
		Public dwMaxBytesPerSec As UInt32
		Public dwFlags As UInt32
		Public dwCaps As UInt32
		Public dwStreams As UInt32
		Public dwSuggestedBufferSize As UInt32
		Public dwWidth As UInt32
		Public dwHeight As UInt32
		Public dwScale As UInt32
		Public dwRate As UInt32
		Public dwLength As UInt32
		Public dwEditCount As UInt32
		<MarshalAs(UnmanagedType.ByValArray, SizeConst:=64)>
		Public szFileType() As UInt16
	End Structure

	''' <summary>AviSaveV needs a pointer to a pointer to an AVICOMPRESSOPTIONS structure</summary>
	<StructLayout(LayoutKind.Sequential, Pack:=1)>
	Public Class AvicompressoptionsClass
		Public fccType As UInt32
		Public fccHandler As UInt32
		Public dwKeyFrameEvery As UInt32 ' only used with AVICOMRPESSF_KEYFRAMES
		Public dwQuality As UInt32
		Public dwBytesPerSecond As UInt32 ' only used with AVICOMPRESSF_DATARATE
		Public dwFlags As UInt32
		Public lpFormat As IntPtr
		Public cbFormat As UInt32
		Public lpParms As IntPtr
		Public cbParms As UInt32
		Public dwInterleaveEvery As UInt32

		Public Function ToStruct() As AVICOMPRESSOPTIONS
			Dim returnVar As New AVICOMPRESSOPTIONS()
			returnVar.fccType = Me.fccType
			returnVar.fccHandler = Me.fccHandler
			returnVar.dwKeyFrameEvery = Me.dwKeyFrameEvery
			returnVar.dwQuality = Me.dwQuality
			returnVar.dwBytesPerSecond = Me.dwBytesPerSecond
			returnVar.dwFlags = Me.dwFlags
			returnVar.lpFormat = Me.lpFormat
			returnVar.cbFormat = Me.cbFormat
			returnVar.lpParms = Me.lpParms
			returnVar.cbParms = Me.cbParms
			returnVar.dwInterleaveEvery = Me.dwInterleaveEvery
			Return returnVar
		End Function
	End Class

	<StructLayout(LayoutKind.Sequential, Pack:=1)>
	Public Structure Avistreaminform
		Public fccType As FssType
		Public fccHandler As UInt32
		Public dwFlags As UInt32
		Public dwCaps As UInt32
		Public wPriority As UInt16
		Public wLanguage As UInt16
		Public dwScale As UInt32
		Public dwRate As UInt32
		Public dwStart As UInt32
		Public dwLength As UInt32
		Public dwInitialFrames As UInt32
		Public dwSuggestedBufferSize As UInt32
		Public dwQuality As Int32
		Public dwSampleSize As UInt32
		Public rcFrame As RECT
		Public dwEditCount As UInt32
		Public dwFormatChangeCount As UInt32
		'			[MarshalAs(UnmanagedType.ByValArray, SizeConst = 64)]
		'			public UInt16[] szName;
		<MarshalAs(UnmanagedType.ByValArray, SizeConst:=64)>
		Public szName() As Char
	End Structure

#End Region

#Region "Functions"
	' Initialize the AVI library.
	<DllImport("avifil32.dll")>
	Public Shared Sub AVIFileInit()
	End Sub

	<DllImport("avifil32.dll")>
	Public Shared Function AVISaveOptions(ByVal hwnd As IntPtr, ByVal uiFlags As UInt32, ByVal nStreams As Int32, ByRef ppavi As IntPtr, ByRef plpOptions As AvicompressoptionsClass) As Boolean
	End Function

	<DllImport("avifil32.dll")>
	Public Shared Function AVISaveOptionsFree(ByVal nStreams As Integer, ByRef plpOptions As AvicompressoptionsClass) As Long
	End Function

    ' Open an AVI file.
    <DllImport("avifil32.dll", PreserveSig:=True)>
    Public Shared Function AVIFileOpen(ByRef ppfile As IntPtr, ByVal szFile As String, ByVal mode As AviFileMode, ByVal pclsidHandler As Integer) As AviErrorCode
    End Function

    <DllImport("avifil32.dll")>
	Public Shared Function AVIMakeCompressedStream(<System.Runtime.InteropServices.Out()> ByRef ppsCompressed As IntPtr, ByVal aviStream As IntPtr, ByRef ao As AVICOMPRESSOPTIONS, ByVal dummy As Integer) As Integer
	End Function

    '' The AVIFileInfo function obtains information about an AVI file.
    '<DllImport("avifil32.dll", PreserveSig:=True)>
    'Public Shared Function AVIFileInfo(ByVal pfile As Integer, ByRef pfi As AVIFILEINFORM, ByVal lSize As Integer) As AVIErrorCode
    'End Function

    '   <DllImport("avifil32.dll", PreserveSig:=True)>
    'Public Shared Function AVIStreamOpenFromFile(<System.Runtime.InteropServices.Out()> ByRef stream As IntPtr, ByVal filename As String, ByVal fccType As FssType, ByVal lParam As Integer, ByVal mode As AVIFileMode, ByVal pclsidHandler As Integer) As AVIErrorCode
    'End Function

    '   'Read the format for a stream
    '   <DllImport("avifil32.dll")>
    'Public Shared Function AVIStreamReadFormat(ByVal aviStream As IntPtr, ByVal lPos As Int32, ByRef lpFormat As BITMAPINFOHEADER, ByRef cbFormat As Int32) As Integer
    'End Function

    '   'Get a stream from an open AVI file
    '   <DllImport("avifil32.dll", PreserveSig:=True)>
    'Public Shared Function AVIFileGetStream(ByVal pfile As Integer, <System.Runtime.InteropServices.Out()> ByRef ppavi As IntPtr, ByVal fccType As FssType, ByVal lParam As Integer) As AVIErrorCode
    'End Function

    '   'Get the start position of a stream
    '   <DllImport("avifil32.dll", PreserveSig:=True)>
    'Public Shared Function AVIStreamStart(ByVal pavi As Integer) As Integer
    'End Function

    '   '    //Get the length of a stream in frames
    '   <DllImport("avifil32.dll", PreserveSig:=True)>
    'Public Shared Function AVIStreamLength(ByVal pavi As Integer) As Integer
    'End Function

    'Release an open AVI stream
    <DllImport("avifil32.dll", PreserveSig:=True)>
	Public Shared Function AVIStreamRelease(ByVal aviStream As IntPtr) As AVIErrorCode
	End Function

	'Get information about an open stream
	<DllImport("avifil32.dll", PreserveSig:=True)>
	Public Shared Function AVIStreamInfo(ByVal pAviStream As IntPtr, ByRef psi As AVISTREAMINFORM, ByVal lSize As Integer) As AVIErrorCode
	End Function

    'Create a new stream in an open AVI file
    <DllImport("avifil32.dll", PreserveSig:=True)>
    Public Shared Function AVIFileCreateStream(ByVal pfile As IntPtr, <System.Runtime.InteropServices.Out()> ByRef ppavi As IntPtr, ByRef ptrStreaminfo As Avistreaminform) As AviErrorCode
    End Function

    'Set the format for a new stream
    '[DllImport("avifil32.dll")]
    'public static extern int AVIStreamSetFormat(IntPtr aviStream, Int32 lPos, ref BITMAPINFOHEADER lpFormat, Int32 cbFormat);

    '   <DllImport("avifil32.dll", PreserveSig:=True)>
    'Public Shared Function AVIStreamSetFormat(ByVal aviStream As IntPtr, ByVal lPos As Int32, ByVal lpFormat As IntPtr, ByVal cbFormat As Int32) As AVIErrorCode
    'End Function

    'Set the format for a new stream
    <DllImport("avifil32.dll")>
	Public Shared Function AVIStreamSetFormat(ByVal aviStream As IntPtr, ByVal lPos As Int32, ByRef lpFormat As BITMAPINFOHEADER, ByVal cbFormat As Int32) As AVIErrorCode
	End Function

    'Release an ope AVI file
    <DllImport("avifil32.dll", PreserveSig:=True)>
    Public Shared Function AVIFileRelease(ByVal pfile As IntPtr) As AviErrorCode
    End Function

    'Close the AVI library
    <DllImport("avifil32.dll")>
	Public Shared Sub AVIFileExit()
	End Sub

    'Get a pointer to a GETFRAME object (returns 0 on error)
    '<DllImport("avifil32.dll")>
    'Public Shared Function AVIStreamGetFrameOpen(ByVal pAviStream As IntPtr, ByVal bih As IntPtr) As UInteger
    'End Function
    'public static extern int AVIStreamGetFrameOpen(IntPtr pAVIStream, out BITMAPINFOHEADER bih);

    'Get a pointer to a packed DIB (returns 0 on error)
    '   <DllImport("avifil32.dll")>
    'Public Shared Function AVIStreamGetFrame(ByVal pGetFrameObj As UInteger, ByVal lPos As Integer) As IntPtr
    'End Function
    '		public static extern uint AVIStreamGetFrame(uint pGetFrameObj, int lPos);

    'Release the GETFRAME object
    '   <DllImport("avifil32.dll")>
    'Public Shared Function AVIStreamGetFrameClose(ByVal pGetFrameObj As UInteger) As UInteger
    'End Function

    '   ''' <summary>Reads audio, video or other data from a stream according to the stream type.</summary>
    '   ''' <param name="aviStream"></param>
    '   ''' <param name="lStart"></param>
    '   ''' <param name="lSamples"></param>
    '   ''' <param name="lpBuffer"></param>
    '   ''' <param name="cbBuffer"></param>
    '   ''' <param name="plBytes"></param>
    '   ''' <param name="plSamples"></param>
    '   ''' <returns></returns>
    '   <DllImport("avifil32.dll")>
    'Public Shared Function AVIStreamRead(ByVal aviStream As IntPtr, ByVal lStart As Integer, ByVal lSamples As Integer, ByVal lpBuffer As IntPtr, ByVal cbBuffer As Integer, <System.Runtime.InteropServices.Out()> ByRef plBytes As Integer, <System.Runtime.InteropServices.Out()> ByRef plSamples As Integer) As AVIErrorCode
    'End Function

    'Write a sample to a stream
    <DllImport("avifil32.dll")>
	Public Shared Function AVIStreamWrite(ByVal aviStream As IntPtr, ByVal lStart As Int32, ByVal lSamples As Int32, ByVal lpBuffer As IntPtr, ByVal cbBuffer As Int32, ByVal dwFlags As Int32, ByVal plSampWritten As Int32, ByVal plBytesWritten As Int32) As UInteger
	End Function

	<DllImport("winmm.dll", EntryPoint:="mmioStringToFOURCCA")>
	Public Shared Function mmioStringToFOURCC(ByVal sz As String, ByVal uFlags As Integer) As UInteger
	End Function

#End Region
End Class
