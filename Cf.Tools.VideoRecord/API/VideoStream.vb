﻿Imports System
Imports System.IO
Imports System.Drawing
Imports System.Drawing.Imaging
Imports System.Runtime.InteropServices
Imports System.Windows.Forms

Friend Class VideoStream
	Inherits AviStream

#Region "Data"

	'Private Shared ReadOnly Log As ILog = LogManager.GetCurrentClassLog()
	''' <summary>stride*height</summary>
	Private _frameSize As UInteger
	Protected _frameRate As Double
	Private _width As Integer
	Private _height As Integer
	Private _countBitsPerPixel As UInt16
	Protected _countFrames As Integer = 0
	Protected _firstFrame As Integer = 0
	Private _compressOptions As AVIAPI.AVICOMPRESSOPTIONS

#End Region

#Region "Properties"

	Public ReadOnly Property FrameSize() As UInteger
		Get
			Return _frameSize
		End Get
	End Property

	Public ReadOnly Property FrameRate() As Double
		Get
			Return _frameRate
		End Get
	End Property

	Public ReadOnly Property Width() As Integer
		Get
			Return _width
		End Get
	End Property

	Public ReadOnly Property Height() As Integer
		Get
			Return _height
		End Get
	End Property

	Public ReadOnly Property CountBitsPerPixel() As UInt16
		Get
			Return _countBitsPerPixel
		End Get
	End Property

	Public ReadOnly Property CountFrames() As Integer
		Get
			Return _countFrames
		End Get
	End Property

	Public ReadOnly Property FirstFrame() As Integer
		Get
			Return _firstFrame
		End Get
	End Property

	Public ReadOnly Property CompressOptions() As AVIAPI.AVICOMPRESSOPTIONS
		Get
			Return _compressOptions
		End Get
	End Property

	Public ReadOnly Property StreamInfo() As AVIAPI.AVISTREAMINFORM
		Get
			Return GetStreamInfo(aviStream)
		End Get
	End Property

#End Region

#Region "Methods"

    ''' <summary>Initialize a new VideoStream and add the first frame</summary>
    ''' <param name="aviFile">The file that contains the stream</param>
    ''' <param name="writeCompressed">true: create a compressed stream before adding frames</param>
    ''' <param name="frameRate">Frames per second</param>
    ''' <param name="firstFrame">Image to write into the stream as the first frame</param>
    Public Sub New(ByVal aviFile As IntPtr, ByVal writeCompressed As Boolean, ByVal frameRate As Double, ByVal firstFrame As Bitmap, ByVal codec As VideoCodecInfo)
        Initialize(aviFile, writeCompressed, frameRate, firstFrame)
        CreateStream(codec)
        AddFrame(firstFrame)
    End Sub

    ''' <summary>Initialize a new VideoStream</summary>
    ''' <remarks>Used only by constructors</remarks>
    ''' <param name="aviFile">The file that contains the stream</param>
    ''' <param name="writeCompressed">true: create a compressed stream before adding frames</param>
    ''' <param name="frameRate">Frames per second</param>
    ''' <param name="firstFrameBitmap">Image to write into the stream as the first frame</param>
    Private Sub Initialize(ByVal aviFile As IntPtr, ByVal writeCompressed As Boolean, ByVal frameRate As Double, ByVal firstFrameBitmap As Bitmap)
        Me.AviFile = aviFile
        Me.WriteCompressedRenamed = writeCompressed
        _frameRate = frameRate
        _firstFrame = 0

        Dim bmpData As BitmapData = firstFrameBitmap.LockBits(New Rectangle(0, 0, firstFrameBitmap.Width, firstFrameBitmap.Height), ImageLockMode.ReadOnly, firstFrameBitmap.PixelFormat)

        _frameSize = Convert.ToUInt32(bmpData.Stride * bmpData.Height)
        _width = firstFrameBitmap.Width
        _height = firstFrameBitmap.Height
        _countBitsPerPixel = ConvertPixelFormatToBitCount(firstFrameBitmap.PixelFormat)

        firstFrameBitmap.UnlockBits(bmpData)
    End Sub

    ''' <summary>Get the count of bits per pixel from a PixelFormat value</summary>
    ''' <param name="format">One of the PixelFormat members beginning with "Format..." - all others are not supported</param>
    ''' <returns>bit count</returns>
    Private Function ConvertPixelFormatToBitCount(ByVal format As PixelFormat) As UInt16
		Dim formatName As String = format.ToString()
		If formatName.Substring(0, 6) <> "Format" Then
			Throw New Exception("Unknown pixel format: " & formatName)
		End If

		formatName = formatName.Substring(6, 2)
		Dim bitCount As UInt16 = 0
		If Char.IsNumber(formatName.Chars(1)) Then
			bitCount = UInt16.Parse(formatName)
		Else
			bitCount = UInt16.Parse(formatName.Chars(0).ToString())
		End If

		Return bitCount
	End Function

	Private Function GetStreamInfo(ByVal aviStream As IntPtr) As AVIAPI.AVISTREAMINFORM
		'INSTANT VB NOTE: The variable streamInfo was renamed since Visual Basic does not handle local variables named the same as class members well:
		Dim streamInfoRenamed As New AVIAPI.AVISTREAMINFORM()
		Dim result = AVIAPI.AVIStreamInfo(StreamPointer, streamInfoRenamed, Marshal.SizeOf(streamInfoRenamed))
		If result <> 0 Then
			Throw New Exception("Exception in VideoStreamInfo: " & result.ToString())
		End If
		Return streamInfoRenamed
	End Function

	Private Sub GetRateAndScale(ByRef frameRate As Double, ByRef scale As Integer)
		scale = 1
		Do While frameRate <> CLng(Math.Truncate(frameRate))
			frameRate = frameRate * 10
			scale *= 10
		Loop
	End Sub

	''' <summary>Create a new stream</summary>
	Private Sub CreateStreamWithoutFormat()
		Dim scale As Integer = 1
		Dim rate As Double = _frameRate
		GetRateAndScale(rate, scale)

		Dim strhdr As New AVIAPI.AVISTREAMINFORM()
		strhdr.fccType = FssType.streamtypeVIDEO 'AVIAPI.mmioStringToFOURCC("vids", 0);
		strhdr.fccHandler = AVIAPI.mmioStringToFOURCC("CVID", 0)
		strhdr.dwFlags = 0
		strhdr.dwCaps = 0
		strhdr.wPriority = 0
		strhdr.wLanguage = 0
		strhdr.dwScale = Convert.ToUInt32(scale)
		strhdr.dwRate = Convert.ToUInt32(rate) ' Frames per Second
		strhdr.dwStart = 0
		strhdr.dwLength = 0
		strhdr.dwInitialFrames = 0
		strhdr.dwSuggestedBufferSize = Convert.ToUInt32(_frameSize)	'height_ * stride_;
		strhdr.dwQuality = -1 'default
		strhdr.dwSampleSize = 0
		strhdr.rcFrame.top = 0
		strhdr.rcFrame.left = 0
		strhdr.rcFrame.bottom = CUInt(_height)
		strhdr.rcFrame.right = CUInt(_width)
		strhdr.dwEditCount = 0
		strhdr.dwFormatChangeCount = 0
		strhdr.szName = New Char(63) {}

		Dim result = AVIAPI.AVIFileCreateStream(aviFile, aviStream, strhdr)

		If result <> 0 Then
			Throw New Exception("Exception in AVIFileCreateStream: " & result.ToString())
		End If
	End Sub

	''' <summary>Create a new stream</summary>
	Private Sub CreateStream(ByVal codec As VideoCodecInfo)
		CreateStreamWithoutFormat()

		If WriteCompressedRenamed Then
			CreateCompressedStream(codec)
		Else
			SetFormat(aviStream)
		End If
	End Sub

	''' <summary>Create a compressed stream from an uncompressed stream</summary>
	Private Sub CreateCompressedStream(ByVal codec As VideoCodecInfo)
		'display the compression options dialog...
		Dim options As New AVIAPI.AvicompressoptionsClass()
		options.fccType = CUInt(FssType.streamtypeVIDEO)

		options.lpParms = IntPtr.Zero
		options.lpFormat = IntPtr.Zero

		'������������ �������� ��������� ������
		'Avi.AVISaveOptions(IntPtr.Zero, Avi.ICMF_CHOOSE_KEYFRAME | Avi.ICMF_CHOOSE_DATARATE, 1, ref aviStream, ref options);
		'Avi.AVISaveOptionsFree(1, ref options);

		'���� ��� ������
		If codec IsNot Nothing Then
			options.fccHandler = codec.FourCC
		Else
			options.fccHandler = VideoCodecInfo.CurrentCodec.FourCC
		End If

		'..or set static options
		'			Avi.AVICOMPRESSOPTIONS opts = new Avi.AVICOMPRESSOPTIONS();
		'			opts.fccType         = (UInt32)Avi.mmioStringToFOURCC("vids", 0);
		'			opts.fccHandler      = (UInt32)Avi.mmioStringToFOURCC("CVID", 0);
		'			opts.dwKeyFrameEvery = 0;
		'			opts.dwQuality       = 0;  // 0 .. 10000
		'			opts.dwFlags         = 0;  // AVICOMRPESSF_KEYFRAMES = 4
		'			opts.dwBytesPerSecond= 0;
		'			opts.lpFormat        = new IntPtr(0);
		'			opts.cbFormat        = 0;
		'			opts.lpParms         = new IntPtr(0);
		'			opts.cbParms         = 0;
		'			opts.dwInterleaveEvery = 0;

		'get the compressed stream
		Me._compressOptions = options.ToStruct()
		Dim result As Integer = AVIAPI.AVIMakeCompressedStream(compressedStream, aviStream, _compressOptions, 0)
		If result <> 0 Then
			Throw New Exception("Exception in AVIMakeCompressedStream: " & result.ToString())
		End If

		SetFormat(compressedStream)
	End Sub

	''' <summary>Add one frame to a new stream</summary>
	''' <param name="bmp"></param>
	''' <remarks>
	''' This works only with uncompressed streams,
	''' and compressed streams that have not been saved yet.
	''' Use DecompressToNewFile to edit saved compressed streams.
	''' </remarks>
	Public Sub AddFrame(ByVal bmp As Bitmap)
		Try
			If bmp Is Nothing OrElse bmp.Width = 0 OrElse bmp.Height = 0 Then
				Return
			End If
            bmp.RotateFlip(RotateFlipType.RotateNoneFlipY)
            Dim bmpDat As BitmapData = bmp.LockBits(New Rectangle(0, 0, bmp.Width, bmp.Height), ImageLockMode.ReadOnly, bmp.PixelFormat)
            Dim result = Aviapi.AVIStreamWrite(If(WriteCompressedRenamed, CompressedStream, StreamPointer), _countFrames, 1, bmpDat.Scan0, CInt(bmpDat.Stride * bmpDat.Height), 0, 0, 0)
            If result <> 0 Then
                Throw New Exception("Exception in VideoStreamWrite: " & result.ToString())
            End If
            bmp.UnlockBits(bmpDat)
            _countFrames += 1
        Catch exc As Exception
            Throw New Exception("VideoStream.AddFrame", exc)
        End Try
	End Sub

	''' <summary>Apply a format to a new stream</summary>
	''' <param name="aviStream">The IAVISTREAM</param>
	''' <remarks>
	''' The format must be set before the first frame can be written,
	''' and it cannot be changed later.
	''' </remarks>
	Private Sub SetFormat(ByVal aviStream As IntPtr)
		Dim bi As New AVIAPI.BITMAPINFOHEADER()
		bi.biSize = CUInt(Marshal.SizeOf(bi))
		bi.biWidth = _width
		bi.biHeight = _height
		bi.biPlanes = 1
		bi.biBitCount = _countBitsPerPixel
		bi.biSizeImage = _frameSize

		Dim result = AVIAPI.AVIStreamSetFormat(aviStream, 0, bi, Convert.ToInt32(bi.biSize))
		If result <> 0 Then
			Throw New Exception("Error in VideoStreamSetFormat: " & result.ToString())
		End If
	End Sub

#End Region
End Class
