﻿Imports System
Imports System.Collections
Imports System.Drawing
Imports System.Drawing.Imaging
Imports System.Runtime.InteropServices

Friend Class AviManager
    Private _aviFile As IntPtr
    Private _streams As New ArrayList()

	''' <summary>Open or create an AVI file</summary>
	''' <param name="fileName">Name of the AVI file</param>
	''' <param name="open">true: Open the file; false: Create or overwrite the file</param>
	Public Sub New(ByVal fileName As String, ByVal open As Boolean)
		AVIAPI.AVIFileInit()
		Dim result As AVIErrorCode
		If open Then
			result = AVIAPI.AVIFileOpen(_aviFile, fileName, AVIFileMode.OfReadwrite, 0)
		Else
			result = AVIAPI.AVIFileOpen(_aviFile, fileName, AVIFileMode.OfWrite Or AVIFileMode.OfCreate, 0)
		End If
		If result <> 0 Then
			Throw New Exception("Exception in AVIFileOpen: " & result.ToString())
		End If
	End Sub

    ''' <summary>Add an empty video stream to the file</summary>
    ''' <param name="isCompressed">true: Create a compressed stream before adding frames</param>
    ''' <param name="frameRate">Frames per second</param>
    ''' <param name="firstFrame">Image to write into the stream as the first frame</param>
    ''' <returns>VideoStream object for the new stream</returns>
    Public Function AddVideoStream(ByVal isCompressed As Boolean,
                                   ByVal frameRate As Double,
                                   ByVal firstFrame As Bitmap,
                                   ByVal codec As VideoCodecInfo) As VideoStream
        Dim stream As New VideoStream(_aviFile,
                                      isCompressed,
                                      frameRate,
                                      firstFrame,
                                      codec)
        _streams.Add(stream)
        Return stream
    End Function

    ''' <summary>Release all ressources</summary>
    Public Sub Close()
		For Each stream As AviStream In _streams
			stream.Close()
		Next stream
		Try
			Do While AVIAPI.AVIFileRelease(_aviFile) <> 0

			Loop
		Catch exc As Exception
			Throw New Exception("AviManager.Close ", exc)
		End Try
		AVIAPI.AVIFileExit()
	End Sub
End Class
