﻿Imports System

Public MustInherit Class AviStream
    Protected AviFile As IntPtr
    Protected aviStream As IntPtr
	Protected CompressedStream As IntPtr
	'INSTANT VB NOTE: The variable writeCompressed was renamed since Visual Basic does not allow variables and other class members to have the same name:
	Protected WriteCompressedRenamed As Boolean

    ''' <summary>Pointer to the unmanaged AVI file</summary>
    Public ReadOnly Property FilePointer() As IntPtr
        Get
            Return AviFile
        End Get
    End Property

    ''' <summary>Pointer to the unmanaged AVI Stream</summary>
    Public Overridable ReadOnly Property StreamPointer() As IntPtr
		Get
			Return aviStream
		End Get
	End Property

	''' <summary>Flag: The stream is compressed/uncompressed</summary>
	Public ReadOnly Property WriteCompressed() As Boolean
		Get
			Return WriteCompressedRenamed
		End Get
	End Property

	''' <summary>Close the stream</summary>
	Public Overridable Sub Close()
		If WriteCompressedRenamed Then
			AVIAPI.AVIStreamRelease(compressedStream)
		End If
		AVIAPI.AVIStreamRelease(StreamPointer)
	End Sub
End Class
