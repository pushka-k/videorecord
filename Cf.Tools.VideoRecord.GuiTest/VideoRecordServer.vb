﻿Imports System.IO
Imports System.Threading
Imports Bwl.Network.ClientServer
Imports Bwl.Framework

Public Class VideoRecordServer
    Implements IDisposable

    Public WithEvents Server As NetServer
    Private ReadOnly _settings As SettingsStorage
    Private ReadOnly _port As IntegerSetting
    Private ReadOnly _videoRecMgr As VideoRecMgrWithSettings
    Private ReadOnly _fpsTime As Integer
    Private ReadOnly _t As Thread
    Private ReadOnly _currFrame As New Dictionary(Of string,Bitmap)
    Private ReadOnly _currDateTime As New Dictionary(Of string, Date)
    Private _logger As Logger

    Public Sub New(app As AppBase)
        _settings = app.RootStorage.CreateChildStorage("VideoRecordServerSettings")
        _port = _settings.CreateIntegerSetting("port", 3670, "Порт")

        _videoRecMgr = New VideoRecMgrWithSettings(app.RootStorage)

        _logger = app.RootLogger

        _logger.CollectLogs(_videoRecMgr)

        Dim fps = _videoRecMgr.FpsSettings.Value
        _fpsTime = 1000 / fps

        _t = New Thread(New ThreadStart(Sub() AddFrameThread()))
        _t.IsBackground = True
        _t.Start()

        Server = New NetServer(_port.Value)
    End Sub

    Private Function PrepareFrame(bmp As Bitmap, dt As DateTime) As Bitmap
        Dim newBmp = New Bitmap(bmp.Clone(), bmp.Width, bmp.Height)
        Dim img As Image = newBmp
        Dim gr = Graphics.FromImage(img)
        Dim text = dt.ToString()
        Dim font As New Font(FontFamily.GenericSansSerif, 72, FontStyle.Regular, GraphicsUnit.Point)
        Dim margin = 0.017
        Dim height As Integer = Int(newBmp.Height * 0.03)
        If bmp.Width < 1000 Then
            height = Int(newBmp.Height * 0.08)
        ElseIf bmp.Width > 2000 Then
            height = Int(newBmp.Height * 0.05)
        End If

        Dim f As Font = FindFont(gr, text, New Size(Int(newBmp.Width - (margin * 2)), height), font)
        Try
            Dim size = gr.MeasureString(text, f)
            Dim rect = New Rectangle(Int(img.Width * margin), Int(img.Height * 0.035), size.Width, size.Height)

            Dim br As New SolidBrush(Color.FromArgb(128, 200, 200, 200))
            ' Specify the text is wrapped.
            gr.FillRectangle(br, rect)
            TextRenderer.DrawText(gr, text, f, rect, Color.FromArgb(150, 0, 0, 0))
        Finally
            font.Dispose()
        End Try
        gr.Save()
        Return img
    End Function

    Private Sub AddFrameThread()
        Do
            Try
                SyncLock (_videoRecMgr)
                    For Each channelIdx In _currFrame.Keys.ToArray
                        Dim srcBmp As Bitmap = Nothing
                        Dim srcDT As Date = Nothing
                        SyncLock _currFrame
                            If _currFrame.Keys.Contains(channelIdx) AndAlso _currFrame(channelIdx) IsNot Nothing Then
                                srcBmp = _currFrame(channelIdx)
                                srcDT = _currDateTime(channelIdx)

                                _currFrame(channelIdx) = Nothing
                                _currDateTime(channelIdx) = Nothing
                            End If
                        End SyncLock

                        If srcBmp IsNot Nothing Then
                            Dim bmp = PrepareFrame(srcBmp, srcDT)
                            _videoRecMgr.AddFrame(channelIdx, bmp)
                        End If
                    Next
                End SyncLock
                Thread.Sleep(_fpsTime)
            Catch ex As Exception
                _logger.AddError("Ошибка при записи видео. Рестарт записи. " + ex.Message)
                Try
                    RestartRecords()
                Catch ex1 As Exception
                End Try
            End Try
        Loop
    End Sub

    Public Sub RestartRecords()
        SyncLock (_videoRecMgr)
            _videoRecMgr.RestartAllRecords()
        End SyncLock
    End Sub

    Private Sub GotImage(message As NetMessage, client As ConnectedClient) Handles Server.ReceivedMessage
        _logger.AddDebug("Получен кадр")

        SyncLock (_videoRecMgr)
            Try
                Dim channelIdx = message.Part(0)
                If Not (_currDateTime.ContainsKey(channelIdx)) Then
                    _currDateTime.Add(channelIdx, New DateTime(Long.Parse(message.Part(1))))
                Else
                    _currDateTime(channelIdx) = New DateTime(Long.Parse(message.Part(1)))
                End If

                Dim stream = New MemoryStream(message.PartBytes(2))
                SyncLock _currFrame
                    If Not (_currFrame.ContainsKey(channelIdx)) Then
                        _currFrame.Add(channelIdx, New Bitmap(stream))
                    Else
                        _currFrame(channelIdx) = New Bitmap(stream)
                    End If
                End SyncLock
                Dim answer = New NetMessage
                answer.Part(0) = "Success"
                client.SendMessage(answer)
            Catch ex As Exception
                _logger.AddError(ex.ToString())
            End Try
        End SyncLock
    End Sub

    private Function FindFont(g As Graphics, longString As String, room As Size, preferedFont as Font) As Font
        Dim resultFont = New Font(preferedFont.FontFamily, preferedFont.SizeInPoints)
        Dim textHeight = room.Height * 2
        While (textHeight > room.Height)
            Dim lines = GetLines(g, longString, room, resultFont)
            textHeight = 0
            For Each line In lines
                textHeight += (g.MeasureString(line, resultFont).Height * 1.15)
            Next

            If (textHeight > room.Height OrElse lines.Count > 1) Then
                resultFont = New Font(resultFont.FontFamily, resultFont.SizeInPoints - 0.5)
            End If
        End While
        Return resultFont
    End Function

    Private Function GetLines(g As Graphics, longString As String, room As Size, preferedFont as Font) As List(Of String)
        Dim words = longString.Split(" ")
        Dim line = ""
        Dim lines = new List (Of String)
        For Each word In words
            If (g.MeasureString(line+" "+word, PreferedFont).Width<room.Width)
                line+=" "+word
                Else 
                lines.Add(Line)
                line = word
            End If
        Next
        If Not String.IsNullOrWhiteSpace(line) Then lines.Add(line)
        return lines
    End Function

#Region "IDisposable Support"

    Private _disposedValue As Boolean ' To detect redundant calls

    ' IDisposable
    Protected Overridable Sub Dispose(disposing As Boolean)
        SyncLock (_videoRecMgr)
            If Not _disposedValue Then
                If disposing Then

                    _videoRecMgr.Dispose()
                    _server.StopServer()
                    _server = nothing

                End If

            End If
            _disposedValue = True
        End SyncLock
    End Sub

    ' TODO: override Finalize() only if Dispose(disposing As Boolean) above has code to free unmanaged resources.
    'Protected Overrides Sub Finalize()
    '    ' Do not change this code.  Put cleanup code in Dispose(disposing As Boolean) above.
    '    Dispose(False)
    '    MyBase.Finalize()
    'End Sub

    ' This code added by Visual Basic to correctly implement the disposable pattern.
    Public Sub Dispose() Implements IDisposable.Dispose
        SyncLock (_videoRecMgr)
            ' Do not change this code.  Put cleanup code in Dispose(disposing As Boolean) above.
            Dispose(True)
            ' TODO: uncomment the following line if Finalize() is overridden above.
            ' GC.SuppressFinalize(Me)
        End SyncLock
    End Sub

#End Region
End Class