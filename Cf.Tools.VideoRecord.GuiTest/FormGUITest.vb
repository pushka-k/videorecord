﻿Imports Bwl.Framework

Public Class FormGuiTest
    Private ReadOnly _frames As New List(Of Bitmap)
    Private _vrm As VideoRecManager
    Private _threadAddFrame As Threading.Thread

    Private ReadOnly _app As New AppBase()
    Private _videoDirectory As StringSetting
    Private _countDays As IntegerSetting
    Private _threadStop As Boolean
    Private _currentVideoChannel As String

    Private Sub Init()
        Dim dir = _app.DataFolder + "frames"
        If IO.Directory.Exists(dir) Then
            Dim files = IO.Directory.GetFiles(dir, "*.jpg")
            For Each file In files
                _frames.Add(Image.FromFile(file))
            Next
        End If
        _videoDirectory = New StringSetting(_app.RootStorage, "VideoDirectory", _app.DataFolder, "Директория с видеозаписями", "")
        _countDays = New IntegerSetting(_app.RootStorage, "DaysDeleteAfter", 50, "Количество дней хранения видеозаписей", "")
        _currentVideoChannel = "ChannelTest"
    End Sub

    Private Sub FormGuiTestLoad(sender As Object, e As EventArgs) Handles MyBase.Load
        Init()
        _tbPath.Text = _videoDirectory.ValueAsString
        _tbCountDays.Text = _countDays.ValueAsString
        _vrm = New VideoRecManager()
        StartAddFrame()
    End Sub

    Private Sub FormGuiTestFormClosing(sender As Object, e As FormClosingEventArgs) Handles MyBase.FormClosing
        CloseAll()
    End Sub

    Private Sub BtnPathClick(sender As Object, e As EventArgs) Handles _btnPath.Click
        Using dialog = New FolderBrowserDialog()
            dialog.RootFolder = Environment.SpecialFolder.MyComputer
            If DialogResult.OK = dialog.ShowDialog Then
                _tbPath.Text = dialog.SelectedPath
            End If
        End Using
    End Sub

    Private Sub Button1Click(sender As Object, e As EventArgs) Handles _btnSelectCodec.Click
        Dim compressStd = VideoCodecInfo.ChooseCompressorStd(IntPtr.Zero)
        If compressStd IsNot Nothing Then
            Dim codecInfo = compressStd.FourCc
            VideoCodecInfo.SetCurrentCodec(codecInfo)
            _tbCodec.Text = codecInfo.ToString
        End If
    End Sub

    Private Sub ThreadAddFrame()
        Do
            SyncLock (_vrm)
                If _vrm IsNot Nothing AndAlso _frames IsNot Nothing Then
                    If _threadStop Then
                        Exit Do
                    End If
                    For Each frame In _frames
                        _vrm.AddFrame(_currentVideoChannel, frame)
                    Next
                End If
            End SyncLock
            If _threadStop Then
                Exit Do
            End If
        Loop
    End Sub

    Private Sub StartAddFrame()
        _threadStop = False
        _threadAddFrame = New Threading.Thread(AddressOf ThreadAddFrame)
        _threadAddFrame.Priority = Threading.ThreadPriority.BelowNormal
        _threadAddFrame.IsBackground = True
        _threadAddFrame.Start()
    End Sub

    Private Sub StopAddFrame()
        _threadStop = True
    End Sub

    'Private Sub ShowVideoRecs()
    '    _listVideoRecs.Items.Clear()
    '    If _vrm IsNot Nothing Then
    '        For Each item In _vrm.VideoRecordings
    '            _listVideoRecs.Items.Add(item.RecordInfo.Name + " [" + item.RecordInfo.Channel + "]")
    '        Next
    '    End If
    'End Sub

    'Private Sub BtnAddVideoRecClick(sender As Object, e As EventArgs) Handles _btnAddVideoRec.Click
    '    If _tbCodec.Text IsNot Nothing AndAlso _tbFPS IsNot Nothing AndAlso _tbTimeRecord IsNot Nothing Then
    '        Dim recInfo = New RecordInfo()
    '        recInfo.Codec = New VideoCodecInfo(_tbCodec.Text, "")
    '        recInfo.Fps = Convert.ToDouble(_tbFPS.Text)
    '        recInfo.Name = _tbName.Text
    '        recInfo.TimeToRecordVideo = Convert.ToInt32(_tbTimeRecord.Text)
    '        recInfo.Channel = _tbChannel.Text
    '        recInfo.Dir = _tbPath.Text
    '        recInfo.HoursDeleteAfter = Convert.ToInt32(_tbCountDays.Text)
    '        If Not _cbVideoChannels.Items.Contains(recInfo.Channel) Then
    '            _cbVideoChannels.Items.Add(recInfo.Channel)
    '            If _cbVideoChannels.Items.Count = 1 Then
    '                _currentVideoChannel = _cbVideoChannels.Items(0)
    '                _cbVideoChannels.Text = _currentVideoChannel
    '            End If
    '        End If
    '        Try
    '            _vrm.AddVideoRec(recInfo)
    '        Catch exc As Exception
    '            MessageBox.Show("Ошибка: " + exc.ToString())
    '        End Try
    '    End If
    '    ShowVideoRecs()
    'End Sub


    Private Sub BtnStartStopThreadClick(sender As Object, e As EventArgs) Handles _btnStartStopThread.Click
        If _threadAddFrame.IsAlive Then
            StopAddFrame()
            _btnStartStopThread.Text = "Старт"
        Else
            StartAddFrame()
            _btnStartStopThread.Text = "Стоп"
        End If
    End Sub

    'Private Sub BtnRemoveClick(sender As Object, e As EventArgs) Handles _btnRemove.Click
    '    Dim selectItem = _listVideoRecs.SelectedItem
    '    If selectItem IsNot Nothing Then
    '        _vrm.RemoveVideoRec(selectItem.ToString())
    '        ShowVideoRecs()
    '    End If
    'End Sub

    'Private Sub CbVideoChannelsSelectedIndexChanged(sender As Object, e As EventArgs) Handles _cbVideoChannels.SelectedIndexChanged
    '    _currentVideoChannel = _cbVideoChannels.Text
    'End Sub

    Private Sub BtnSetupClick(sender As Object, e As EventArgs) Handles _btnSetup.Click
        _app.RootStorage.ShowSettingsForm(Me)
        _app.RootStorage.SaveSettings()
    End Sub

    Private Sub CloseAll()
        StopAddFrame()
        _vrm.Dispose()
    End Sub

End Class
