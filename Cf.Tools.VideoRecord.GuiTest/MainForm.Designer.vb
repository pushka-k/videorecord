﻿Imports Bwl.Framework

<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class MainForm
    Inherits FormAppBase

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Dim resources As System.ComponentModel.ComponentResourceManager = New System.ComponentModel.ComponentResourceManager(GetType(MainForm))
        Me._btnCloseCurrentRecord = New System.Windows.Forms.Button()
        Me.SuspendLayout()
        '
        'logWriter
        '
        Me.logWriter.Anchor = CType((((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Bottom) _
            Or System.Windows.Forms.AnchorStyles.Left) _
            Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.logWriter.Location = New System.Drawing.Point(0, 69)
        Me.logWriter.Size = New System.Drawing.Size(420, 153)
        '
        '_btnCloseCurrentRecord
        '
        Me._btnCloseCurrentRecord.Location = New System.Drawing.Point(12, 27)
        Me._btnCloseCurrentRecord.Name = "_btnCloseCurrentRecord"
        Me._btnCloseCurrentRecord.Size = New System.Drawing.Size(223, 23)
        Me._btnCloseCurrentRecord.TabIndex = 2
        Me._btnCloseCurrentRecord.Text = "Остановить текущую запись видео"
        Me._btnCloseCurrentRecord.UseVisualStyleBackColor = True
        '
        'MainForm
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.ClientSize = New System.Drawing.Size(420, 221)
        Me.Controls.Add(Me._btnCloseCurrentRecord)
        Me.Icon = CType(resources.GetObject("$this.Icon"), System.Drawing.Icon)
        Me.Name = "MainForm"
        Me.Text = "MainForm"
        Me.Controls.SetChildIndex(Me.logWriter, 0)
        Me.Controls.SetChildIndex(Me._btnCloseCurrentRecord, 0)
        Me.ResumeLayout(False)
        Me.PerformLayout

End Sub

    Friend WithEvents _btnCloseCurrentRecord As Button
End Class
