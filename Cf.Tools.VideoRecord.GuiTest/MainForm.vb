﻿Public Class MainForm

    Private _server As VideoRecordServer

    Public Sub New()

        ' This call is required by the designer.
        InitializeComponent()

        ' Add any initialization after the InitializeComponent() call.
        '_server = New VideoRecordServer(AppBase)


        Dim videoRecManager = New VideoRecMgrWithSettings(AppBase.RootStorage)
        Dim bmp = New Bitmap(300, 600)

        For index = 1 To 100
            videoRecManager.AddFrame(1, bmp)
        Next

        videoRecManager.CloseAllRecords()

        For index = 1 To 100
            videoRecManager.AddFrame(1, bmp)
        Next


        videoRecManager.Dispose()
    End Sub

    'Private Sub MainFormFormClosing(sender As Object, e As FormClosingEventArgs) Handles MyBase.FormClosing
    '    _server.Dispose()
    'End Sub

    Private Sub _btnCloseCurrentRecord_Click(sender As Object, e As EventArgs) Handles _btnCloseCurrentRecord.Click
        _server.RestartRecords()
    End Sub
End Class