<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()>
Partial Class FormGuiTest
    Inherits System.Windows.Forms.Form

    'Форма переопределяет dispose для очистки списка компонентов.
    <System.Diagnostics.DebuggerNonUserCode()>
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Является обязательной для конструктора форм Windows Forms
    Private components As System.ComponentModel.IContainer

    'Примечание: следующая процедура является обязательной для конструктора форм Windows Forms
    'Для ее изменения используйте конструктор форм Windows Form.  
    'Не изменяйте ее в редакторе исходного кода.
    <System.Diagnostics.DebuggerStepThrough()>
    Private Sub InitializeComponent()
        Me.components = New System.ComponentModel.Container()
        Me._btnPath = New System.Windows.Forms.Button()
        Me._tbPath = New System.Windows.Forms.TextBox()
        Me._timerAddFrame = New System.Windows.Forms.Timer(Me.components)
        Me._listBitmap = New System.Windows.Forms.ImageList(Me.components)
        Me._tbFPS = New System.Windows.Forms.TextBox()
        Me._labelCodec = New System.Windows.Forms.Label()
        Me._btnSelectCodec = New System.Windows.Forms.Button()
        Me._tbCodec = New System.Windows.Forms.TextBox()
        Me._btnAddVideoRec = New System.Windows.Forms.Button()
        Me._listVideoRecs = New System.Windows.Forms.ListBox()
        Me.Panel1 = New System.Windows.Forms.Panel()
        Me._cbVideoChannels = New System.Windows.Forms.ComboBox()
        Me._labelChannelRec = New System.Windows.Forms.Label()
        Me._btnRemove = New System.Windows.Forms.Button()
        Me._labelChannel = New System.Windows.Forms.Label()
        Me._tbChannel = New System.Windows.Forms.TextBox()
        Me._btnStartStopThread = New System.Windows.Forms.Button()
        Me._labelName = New System.Windows.Forms.Label()
        Me._tbName = New System.Windows.Forms.TextBox()
        Me._btnSetup = New System.Windows.Forms.Button()
        Me._labelHour = New System.Windows.Forms.Label()
        Me._labelCountDays = New System.Windows.Forms.Label()
        Me._tbCountDays = New System.Windows.Forms.TextBox()
        Me._labelTimeRecord = New System.Windows.Forms.Label()
        Me._tbTimeRecord = New System.Windows.Forms.TextBox()
        Me._labelDirectory = New System.Windows.Forms.Label()
        Me._labelFPS = New System.Windows.Forms.Label()
        Me.Panel1.SuspendLayout()
        Me.SuspendLayout()
        '
        '_btnPath
        '
        Me._btnPath.Anchor = CType((System.Windows.Forms.AnchorStyles.Bottom Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me._btnPath.Location = New System.Drawing.Point(254, 3)
        Me._btnPath.Name = "_btnPath"
        Me._btnPath.Size = New System.Drawing.Size(100, 23)
        Me._btnPath.TabIndex = 3
        Me._btnPath.Text = "Обзор..."
        Me._btnPath.UseVisualStyleBackColor = True
        '
        '_tbPath
        '
        Me._tbPath.Anchor = CType(((System.Windows.Forms.AnchorStyles.Bottom Or System.Windows.Forms.AnchorStyles.Left) _
            Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me._tbPath.Location = New System.Drawing.Point(94, 3)
        Me._tbPath.Name = "_tbPath"
        Me._tbPath.Size = New System.Drawing.Size(156, 20)
        Me._tbPath.TabIndex = 4
        '
        '_timerAddFrame
        '
        Me._timerAddFrame.Interval = 1000
        '
        '_listBitmap
        '
        Me._listBitmap.ColorDepth = System.Windows.Forms.ColorDepth.Depth8Bit
        Me._listBitmap.ImageSize = New System.Drawing.Size(50, 50)
        Me._listBitmap.TransparentColor = System.Drawing.Color.Transparent
        '
        '_tbFPS
        '
        Me._tbFPS.Anchor = CType(((System.Windows.Forms.AnchorStyles.Bottom Or System.Windows.Forms.AnchorStyles.Left) _
            Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me._tbFPS.Location = New System.Drawing.Point(94, 107)
        Me._tbFPS.Name = "_tbFPS"
        Me._tbFPS.Size = New System.Drawing.Size(20, 20)
        Me._tbFPS.TabIndex = 8
        Me._tbFPS.Text = "2"
        '
        '_labelCodec
        '
        Me._labelCodec.Anchor = CType(((System.Windows.Forms.AnchorStyles.Bottom Or System.Windows.Forms.AnchorStyles.Left) _
            Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me._labelCodec.AutoSize = True
        Me._labelCodec.Location = New System.Drawing.Point(6, 136)
        Me._labelCodec.Name = "_labelCodec"
        Me._labelCodec.Size = New System.Drawing.Size(41, 13)
        Me._labelCodec.TabIndex = 11
        Me._labelCodec.Text = "Кодек:"
        '
        '_btnSelectCodec
        '
        Me._btnSelectCodec.Anchor = CType((System.Windows.Forms.AnchorStyles.Bottom Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me._btnSelectCodec.Location = New System.Drawing.Point(254, 133)
        Me._btnSelectCodec.Name = "_btnSelectCodec"
        Me._btnSelectCodec.Size = New System.Drawing.Size(100, 23)
        Me._btnSelectCodec.TabIndex = 13
        Me._btnSelectCodec.Text = "Выбрать"
        Me._btnSelectCodec.UseVisualStyleBackColor = True
        '
        '_tbCodec
        '
        Me._tbCodec.Anchor = CType(((System.Windows.Forms.AnchorStyles.Bottom Or System.Windows.Forms.AnchorStyles.Left) _
            Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me._tbCodec.Location = New System.Drawing.Point(94, 133)
        Me._tbCodec.Name = "_tbCodec"
        Me._tbCodec.Size = New System.Drawing.Size(156, 20)
        Me._tbCodec.TabIndex = 14
        Me._tbCodec.Text = "1684633208"
        '
        '_btnAddVideoRec
        '
        Me._btnAddVideoRec.Anchor = CType((System.Windows.Forms.AnchorStyles.Bottom Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me._btnAddVideoRec.Location = New System.Drawing.Point(254, 237)
        Me._btnAddVideoRec.Name = "_btnAddVideoRec"
        Me._btnAddVideoRec.Size = New System.Drawing.Size(100, 23)
        Me._btnAddVideoRec.TabIndex = 15
        Me._btnAddVideoRec.Text = "Добавить"
        Me._btnAddVideoRec.UseVisualStyleBackColor = True
        '
        '_listVideoRecs
        '
        Me._listVideoRecs.Anchor = CType(((System.Windows.Forms.AnchorStyles.Bottom Or System.Windows.Forms.AnchorStyles.Left) _
            Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me._listVideoRecs.FormattingEnabled = True
        Me._listVideoRecs.Location = New System.Drawing.Point(9, 328)
        Me._listVideoRecs.Name = "_listVideoRecs"
        Me._listVideoRecs.Size = New System.Drawing.Size(241, 95)
        Me._listVideoRecs.TabIndex = 16
        '
        'Panel1
        '
        Me.Panel1.Anchor = CType((((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Bottom) _
            Or System.Windows.Forms.AnchorStyles.Left) _
            Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.Panel1.BackColor = System.Drawing.SystemColors.Control
        Me.Panel1.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.Panel1.Controls.Add(Me._cbVideoChannels)
        Me.Panel1.Controls.Add(Me._labelChannelRec)
        Me.Panel1.Controls.Add(Me._btnRemove)
        Me.Panel1.Controls.Add(Me._labelChannel)
        Me.Panel1.Controls.Add(Me._tbChannel)
        Me.Panel1.Controls.Add(Me._btnStartStopThread)
        Me.Panel1.Controls.Add(Me._labelName)
        Me.Panel1.Controls.Add(Me._tbName)
        Me.Panel1.Controls.Add(Me._btnSelectCodec)
        Me.Panel1.Controls.Add(Me._tbCodec)
        Me.Panel1.Controls.Add(Me._btnSetup)
        Me.Panel1.Controls.Add(Me._labelCodec)
        Me.Panel1.Controls.Add(Me._labelHour)
        Me.Panel1.Controls.Add(Me._btnPath)
        Me.Panel1.Controls.Add(Me._tbPath)
        Me.Panel1.Controls.Add(Me._labelCountDays)
        Me.Panel1.Controls.Add(Me._tbFPS)
        Me.Panel1.Controls.Add(Me._tbCountDays)
        Me.Panel1.Controls.Add(Me._labelTimeRecord)
        Me.Panel1.Controls.Add(Me._tbTimeRecord)
        Me.Panel1.Controls.Add(Me._labelDirectory)
        Me.Panel1.Controls.Add(Me._labelFPS)
        Me.Panel1.Controls.Add(Me._btnAddVideoRec)
        Me.Panel1.Controls.Add(Me._listVideoRecs)
        Me.Panel1.Location = New System.Drawing.Point(12, 12)
        Me.Panel1.Name = "Panel1"
        Me.Panel1.Size = New System.Drawing.Size(360, 438)
        Me.Panel1.TabIndex = 17
        '
        '_cbVideoChannels
        '
        Me._cbVideoChannels.FormattingEnabled = True
        Me._cbVideoChannels.Location = New System.Drawing.Point(131, 299)
        Me._cbVideoChannels.Name = "_cbVideoChannels"
        Me._cbVideoChannels.Size = New System.Drawing.Size(119, 21)
        Me._cbVideoChannels.TabIndex = 36
        '
        '_labelChannelRec
        '
        Me._labelChannelRec.Anchor = CType(((System.Windows.Forms.AnchorStyles.Bottom Or System.Windows.Forms.AnchorStyles.Left) _
            Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me._labelChannelRec.AutoSize = True
        Me._labelChannelRec.Location = New System.Drawing.Point(7, 302)
        Me._labelChannelRec.Name = "_labelChannelRec"
        Me._labelChannelRec.Size = New System.Drawing.Size(119, 13)
        Me._labelChannelRec.TabIndex = 35
        Me._labelChannelRec.Text = "Запись в видеоканал:"
        '
        '_btnRemove
        '
        Me._btnRemove.Anchor = CType((System.Windows.Forms.AnchorStyles.Bottom Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me._btnRemove.Location = New System.Drawing.Point(254, 328)
        Me._btnRemove.Name = "_btnRemove"
        Me._btnRemove.Size = New System.Drawing.Size(100, 23)
        Me._btnRemove.TabIndex = 34
        Me._btnRemove.Text = "Удалить"
        Me._btnRemove.UseVisualStyleBackColor = True
        '
        '_labelChannel
        '
        Me._labelChannel.Anchor = CType(((System.Windows.Forms.AnchorStyles.Bottom Or System.Windows.Forms.AnchorStyles.Left) _
            Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me._labelChannel.AutoSize = True
        Me._labelChannel.Location = New System.Drawing.Point(6, 214)
        Me._labelChannel.Name = "_labelChannel"
        Me._labelChannel.Size = New System.Drawing.Size(71, 13)
        Me._labelChannel.TabIndex = 33
        Me._labelChannel.Text = "Видеоканал:"
        '
        '_tbChannel
        '
        Me._tbChannel.Anchor = CType(((System.Windows.Forms.AnchorStyles.Bottom Or System.Windows.Forms.AnchorStyles.Left) _
            Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me._tbChannel.Location = New System.Drawing.Point(94, 211)
        Me._tbChannel.Name = "_tbChannel"
        Me._tbChannel.Size = New System.Drawing.Size(156, 20)
        Me._tbChannel.TabIndex = 32
        Me._tbChannel.Text = "videochannel1"
        '
        '_btnStartStopThread
        '
        Me._btnStartStopThread.Anchor = CType((System.Windows.Forms.AnchorStyles.Bottom Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me._btnStartStopThread.Location = New System.Drawing.Point(254, 357)
        Me._btnStartStopThread.Name = "_btnStartStopThread"
        Me._btnStartStopThread.Size = New System.Drawing.Size(100, 23)
        Me._btnStartStopThread.TabIndex = 31
        Me._btnStartStopThread.Text = "Стоп"
        Me._btnStartStopThread.UseVisualStyleBackColor = True
        '
        '_labelName
        '
        Me._labelName.Anchor = CType(((System.Windows.Forms.AnchorStyles.Bottom Or System.Windows.Forms.AnchorStyles.Left) _
            Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me._labelName.AutoSize = True
        Me._labelName.Location = New System.Drawing.Point(6, 188)
        Me._labelName.Name = "_labelName"
        Me._labelName.Size = New System.Drawing.Size(60, 13)
        Me._labelName.TabIndex = 30
        Me._labelName.Text = "Название:"
        '
        '_tbName
        '
        Me._tbName.Anchor = CType(((System.Windows.Forms.AnchorStyles.Bottom Or System.Windows.Forms.AnchorStyles.Left) _
            Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me._tbName.Location = New System.Drawing.Point(94, 185)
        Me._tbName.Name = "_tbName"
        Me._tbName.Size = New System.Drawing.Size(156, 20)
        Me._tbName.TabIndex = 29
        '
        '_btnSetup
        '
        Me._btnSetup.Anchor = CType((System.Windows.Forms.AnchorStyles.Bottom Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me._btnSetup.Location = New System.Drawing.Point(254, 57)
        Me._btnSetup.Name = "_btnSetup"
        Me._btnSetup.Size = New System.Drawing.Size(100, 23)
        Me._btnSetup.TabIndex = 28
        Me._btnSetup.Text = "Настройки"
        Me._btnSetup.UseVisualStyleBackColor = True
        '
        '_labelHour
        '
        Me._labelHour.Anchor = CType((System.Windows.Forms.AnchorStyles.Bottom Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me._labelHour.AutoSize = True
        Me._labelHour.Location = New System.Drawing.Point(254, 164)
        Me._labelHour.Name = "_labelHour"
        Me._labelHour.Size = New System.Drawing.Size(33, 13)
        Me._labelHour.TabIndex = 27
        Me._labelHour.Text = "(час.)"
        '
        '_labelCountDays
        '
        Me._labelCountDays.Anchor = CType(((System.Windows.Forms.AnchorStyles.Bottom Or System.Windows.Forms.AnchorStyles.Left) _
            Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me._labelCountDays.AutoSize = True
        Me._labelCountDays.Location = New System.Drawing.Point(6, 32)
        Me._labelCountDays.Name = "_labelCountDays"
        Me._labelCountDays.Size = New System.Drawing.Size(71, 13)
        Me._labelCountDays.TabIndex = 25
        Me._labelCountDays.Text = "Кол-во дней:"
        '
        '_tbCountDays
        '
        Me._tbCountDays.Anchor = CType(((System.Windows.Forms.AnchorStyles.Bottom Or System.Windows.Forms.AnchorStyles.Left) _
            Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me._tbCountDays.Location = New System.Drawing.Point(94, 29)
        Me._tbCountDays.Name = "_tbCountDays"
        Me._tbCountDays.Size = New System.Drawing.Size(156, 20)
        Me._tbCountDays.TabIndex = 24
        '
        '_labelTimeRecord
        '
        Me._labelTimeRecord.Anchor = CType(((System.Windows.Forms.AnchorStyles.Bottom Or System.Windows.Forms.AnchorStyles.Left) _
            Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me._labelTimeRecord.AutoSize = True
        Me._labelTimeRecord.Location = New System.Drawing.Point(6, 162)
        Me._labelTimeRecord.Name = "_labelTimeRecord"
        Me._labelTimeRecord.Size = New System.Drawing.Size(82, 13)
        Me._labelTimeRecord.TabIndex = 23
        Me._labelTimeRecord.Text = "Время записи:"
        '
        '_tbTimeRecord
        '
        Me._tbTimeRecord.Anchor = CType(((System.Windows.Forms.AnchorStyles.Bottom Or System.Windows.Forms.AnchorStyles.Left) _
            Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me._tbTimeRecord.Location = New System.Drawing.Point(94, 159)
        Me._tbTimeRecord.Name = "_tbTimeRecord"
        Me._tbTimeRecord.Size = New System.Drawing.Size(156, 20)
        Me._tbTimeRecord.TabIndex = 22
        Me._tbTimeRecord.Text = "1"
        '
        '_labelDirectory
        '
        Me._labelDirectory.Anchor = CType(((System.Windows.Forms.AnchorStyles.Bottom Or System.Windows.Forms.AnchorStyles.Left) _
            Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me._labelDirectory.AutoSize = True
        Me._labelDirectory.Location = New System.Drawing.Point(6, 6)
        Me._labelDirectory.Name = "_labelDirectory"
        Me._labelDirectory.Size = New System.Drawing.Size(72, 13)
        Me._labelDirectory.TabIndex = 20
        Me._labelDirectory.Text = "Директория:"
        '
        '_labelFPS
        '
        Me._labelFPS.Anchor = CType(((System.Windows.Forms.AnchorStyles.Bottom Or System.Windows.Forms.AnchorStyles.Left) _
            Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me._labelFPS.AutoSize = True
        Me._labelFPS.Location = New System.Drawing.Point(6, 110)
        Me._labelFPS.Name = "_labelFPS"
        Me._labelFPS.Size = New System.Drawing.Size(24, 13)
        Me._labelFPS.TabIndex = 18
        Me._labelFPS.Text = "fps:"
        '
        'FormGuiTest
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.ClientSize = New System.Drawing.Size(384, 462)
        Me.Controls.Add(Me.Panel1)
        Me.MaximumSize = New System.Drawing.Size(1000, 500)
        Me.MinimumSize = New System.Drawing.Size(400, 500)
        Me.Name = "FormGuiTest"
        Me.Text = "Cf.Tools.VideoRecord.GuiTest"
		Me.Panel1.ResumeLayout(False)
		Me.Panel1.PerformLayout()
		Me.ResumeLayout(False)

	End Sub
	Private WithEvents _btnPath As System.Windows.Forms.Button
	Private WithEvents _tbPath As System.Windows.Forms.TextBox
	Private WithEvents _timerAddFrame As System.Windows.Forms.Timer
	Private WithEvents _listBitmap As System.Windows.Forms.ImageList
	Private WithEvents _tbFPS As System.Windows.Forms.TextBox
	Private WithEvents _labelCodec As System.Windows.Forms.Label
	Private WithEvents _btnSelectCodec As System.Windows.Forms.Button
	Private WithEvents _tbCodec As System.Windows.Forms.TextBox
	Private WithEvents _btnAddVideoRec As System.Windows.Forms.Button
	Private WithEvents _listVideoRecs As System.Windows.Forms.ListBox
	Private WithEvents Panel1 As System.Windows.Forms.Panel
	Private WithEvents _labelHour As System.Windows.Forms.Label
	Private WithEvents _labelCountDays As System.Windows.Forms.Label
	Private WithEvents _tbCountDays As System.Windows.Forms.TextBox
	Private WithEvents _labelTimeRecord As System.Windows.Forms.Label
	Private WithEvents _tbTimeRecord As System.Windows.Forms.TextBox
	Private WithEvents _labelDirectory As System.Windows.Forms.Label
	Private WithEvents _labelFPS As System.Windows.Forms.Label
	Private WithEvents _btnSetup As System.Windows.Forms.Button
	Private WithEvents _labelName As System.Windows.Forms.Label
	Private WithEvents _tbName As System.Windows.Forms.TextBox
	Private WithEvents _btnStartStopThread As System.Windows.Forms.Button
	Private WithEvents _labelChannel As System.Windows.Forms.Label
	Private WithEvents _tbChannel As System.Windows.Forms.TextBox
	Private WithEvents _btnRemove As System.Windows.Forms.Button
	Friend WithEvents _cbVideoChannels As System.Windows.Forms.ComboBox
	Private WithEvents _labelChannelRec As System.Windows.Forms.Label


End Class
