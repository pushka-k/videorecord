﻿Imports System.IO
Imports Bwl.Framework

Public Class VideoRecMgrWithSettings
    Inherits VideoRecManager

    Public Property CodecSettings As StringSetting
    Public Property DirectoryForSavingSettings As StringSetting
    Public Property TimeToStoreVideoHoursSettings As IntegerSetting
    Public Property VideoLengthMinutesSettings As IntegerSetting
    Public Property FpsSettings As DoubleSetting
    Public Property CodecSetupGUISettings As BooleanSetting

    Public Sub New(settings As SettingsStorage)
        CodecSettings = settings.CreateStringSetting("codec", "1684633208")
        CodecSetupGUISettings = settings.CreateBooleanSetting("CodecSetupGUI", False)
        DirectoryForSavingSettings = settings.CreateStringSetting("directoryForSaving", Path.Combine(AppDomain.CurrentDomain.BaseDirectory, "..", "video"))
        TimeToStoreVideoHoursSettings = settings.CreateIntegerSetting("timeToStoreVideoHours", 24)
        VideoLengthMinutesSettings = settings.CreateIntegerSetting("videoLengthMinutes", 20)
        FpsSettings = settings.CreateDoubleSetting("fps", 5)

        AddHandler CodecSettings.ValueChanged, AddressOf ApplySettings
        AddHandler DirectoryForSavingSettings.ValueChanged, AddressOf ApplySettings
        AddHandler TimeToStoreVideoHoursSettings.ValueChanged, AddressOf ApplySettings
        AddHandler VideoLengthMinutesSettings.ValueChanged, AddressOf ApplySettings
        AddHandler FpsSettings.ValueChanged, AddressOf ApplySettings

        AddHandler CodecSetupGUISettings.ValueChanged, AddressOf CodecSetupGUI

        ApplySettings()
    End Sub

    Private Sub ApplySettings()
        Codec = CodecSettings.Value
        DirectoryForSaving = DirectoryForSavingSettings.Value
        VideoLengthMinutes = VideoLengthMinutesSettings.Value
        TimeToStoreVideoHours = TimeToStoreVideoHoursSettings.Value
        Fps = FpsSettings.Value
    End Sub

    Private Sub CodecSetupGUI()
        Dim compressStd = VideoCodecInfo.ChooseCompressorStd(IntPtr.Zero)
        If compressStd IsNot Nothing Then
            Dim codecInfo = compressStd.FourCc
            VideoCodecInfo.SetCurrentCodec(codecInfo)
            CodecSettings.Value = codecInfo.ToString
        End If
    End Sub

End Class
